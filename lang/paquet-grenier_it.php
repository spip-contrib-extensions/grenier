<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'Funzioni e API deprecate',
	'grenier_slogan' => 'Funzioni e API di SPIP deprecate',
];
