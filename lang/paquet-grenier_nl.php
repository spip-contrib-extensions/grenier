<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'Gedepreciëerde functie en API',
	'grenier_slogan' => 'Gedepreciëerde functie en SPIP API',
];
