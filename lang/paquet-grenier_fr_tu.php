<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=fr_tu
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'Fonctions et API dépréciées',
	'grenier_slogan' => 'Fonctions et API SPIP dépréciées',
];
