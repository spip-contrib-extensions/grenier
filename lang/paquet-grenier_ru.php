<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'Поддержка API и функций предыдущих версий',
	'grenier_slogan' => 'Поддержка API и функций предыдущих версий',
];
