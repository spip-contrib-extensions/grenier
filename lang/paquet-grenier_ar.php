<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'وظائف عفى عليها الزمن',
	'grenier_slogan' => 'وظائف SPIP عفى عليها الزمن',
];
