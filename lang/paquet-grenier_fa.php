<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=fa
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'كاركردها و اي.پي.آي‌هاي رد شده',
	'grenier_slogan' => 'كاركردها و اي.پي.آي اسپيپ رد شده ',
];
