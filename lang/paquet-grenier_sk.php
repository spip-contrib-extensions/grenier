<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-grenier?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// G
	'grenier_description' => 'Funkcie a zastarané aplikácie',
	'grenier_slogan' => 'Funkcie a zastarané aplikácie SPIPu',
];
